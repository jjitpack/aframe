package com.xj.myframe;

import android.os.Handler;
import android.view.View;

import com.xj.frame.base.mvp.BaseMvpVBActivity;
import com.xj.frame.utils.ActivitySkipUtils;
import com.xj.myframe.databinding.ActivityMainBinding;

public class MainActivity extends BaseMvpVBActivity< ActivityMainBinding, MainPresenter > {

    @Override
    protected void initView() {
        getTvTitle().setText("xx标题");
        getTvTitleRight().setText("next");
        getTvTitleRight().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivitySkipUtils.launchActivity(MainActivity.this, TestActivity.class);
            }
        });
    }

    @Override
    protected void initData() {
        viewBinding.tvAaa.setText("gggh---hhh");

//        presenter

        showLoadingPop();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dismissLoadingPop();
            }
        }, 2000);
    }
}