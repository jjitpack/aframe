package com.xj.myframe.home;

import android.Manifest;
import android.os.Handler;
import android.util.Log;

import androidx.annotation.NonNull;

import com.github.dfqin.grantor.PermissionListener;
import com.github.dfqin.grantor.PermissionsUtil;
import com.xj.frame.base.BaseVBActivity;
import com.xj.frame.utils.ActivitySkipUtils;
import com.xj.myframe.MainActivity;
import com.xj.myframe.databinding.ActivitySplashBinding;

public class SplashActivity extends BaseVBActivity< ActivitySplashBinding > {

    @Override
    protected void initView() {
        hideDefaultTitle();
    }

    @Override
    protected void initData() {
        PermissionsUtil.requestPermission(this, new PermissionListener() {
                    @Override
                    public void permissionGranted(@NonNull String[] permission) {
                        Log.i("xxx===", "已开启权限");
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                ActivitySkipUtils.launchActivity(SplashActivity.this, MainActivity.class);
                            }
                        }, 2000);
                    }

                    @Override
                    public void permissionDenied(@NonNull String[] permission) {

                        Log.i("xxx===", "没开启权限");
                    }
                }, Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.CAMERA,
                Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }
}