package com.xj.myframe;

import com.xj.frame.base.BaseVBActivity;
import com.xj.myframe.databinding.ActivityTestBinding;

public class TestActivity extends BaseVBActivity< ActivityTestBinding > {

    @Override
    protected void initView() {
        hideDefaultTitle();
        viewBinding.tvTest.setText("fddaaa哈哈");
    }

    @Override
    protected void initData() {

    }
}