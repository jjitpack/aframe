package com.xj.myframe;

/**
 * 版本信息
 * @author gl
 * @time 2021/6/30 19:46
 */
public class AppVersion {

    private int id;
    private String pdaTypeName;//	类型名称	string
    private String appName;//	app名称	string
    private String appVersion;//	app版本	string
    private String upgradeSourceAddress;//	升级源地址	string
    private String upgradeExplain;//	更新升级说明	string
    private String upgradeTime;//	更新时间	string
    private int pdaTypeId;//	类型id	integer
    private String upgradeFlag;//	0 必要升级 1 不必要升级	string

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPdaTypeName() {
        return pdaTypeName;
    }

    public void setPdaTypeName(String pdaTypeName) {
        this.pdaTypeName = pdaTypeName;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getUpgradeSourceAddress() {
        return upgradeSourceAddress;
    }

    public void setUpgradeSourceAddress(String upgradeSourceAddress) {
        this.upgradeSourceAddress = upgradeSourceAddress;
    }

    public String getUpgradeExplain() {
        return upgradeExplain;
    }

    public void setUpgradeExplain(String upgradeExplain) {
        this.upgradeExplain = upgradeExplain;
    }

    public String getUpgradeTime() {
        return upgradeTime;
    }

    public void setUpgradeTime(String upgradeTime) {
        this.upgradeTime = upgradeTime;
    }

    public int getPdaTypeId() {
        return pdaTypeId;
    }

    public void setPdaTypeId(int pdaTypeId) {
        this.pdaTypeId = pdaTypeId;
    }

    public String getUpgradeFlag() {
        return upgradeFlag;
    }

    public void setUpgradeFlag(String upgradeFlag) {
        this.upgradeFlag = upgradeFlag;
    }
}
