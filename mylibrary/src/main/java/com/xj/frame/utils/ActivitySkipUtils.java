package com.xj.frame.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public class ActivitySkipUtils {

    public static void launchActivity(Context context, Class aClass) {
        context.startActivity(getIntent(context, aClass, null));
    }

    public static void launchActivity(Context context, Class aClass, Bundle bundle) {
        context.startActivity(getIntent(context, aClass, bundle));
    }

    public static void launchActivityForResult(Activity context, Class aClass, Bundle bundle, int requestCode) {
        context.startActivityForResult(getIntent(context, aClass, bundle), requestCode);
        context.finish();
    }

    private static Intent getIntent(Context context, Class aClass, Bundle bundle) {
        Intent intent = new Intent(context.getApplicationContext(), aClass);
        if (!(context instanceof Activity)) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        if (null != bundle) {
            intent.putExtras(bundle);
        }
        return intent;
    }

}
