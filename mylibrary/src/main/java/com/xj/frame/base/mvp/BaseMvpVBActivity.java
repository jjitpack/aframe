package com.xj.frame.base.mvp;

import androidx.viewbinding.ViewBinding;

import android.view.View;

import com.xj.frame.base.BaseVBActivity;

import java.lang.reflect.ParameterizedType;

public abstract class BaseMvpVBActivity< VB extends ViewBinding, P extends BasePresenter > extends BaseVBActivity< VB > implements IView {
    public P presenter;

    @Override
    protected View getContentView() {
        View view = super.getContentView();
        presenter = createPresenter();
        return view;
    }

    @SuppressWarnings("unchecked")
    protected P createPresenter() {
        P presenter = null;
        // 获取当前对象的父类类型
        ParameterizedType pt = (ParameterizedType) (this.getClass().getGenericSuperclass());
        if (pt != null) {
            // 获取第一个类型参数的真实类型
            Class< P > pClass = (Class< P >) pt.getActualTypeArguments()[1];
            ParameterizedType ppt = (ParameterizedType) pClass.getGenericSuperclass();
            try {
                presenter = pClass.getConstructor((Class< ? extends IView >) ppt.getActualTypeArguments()[0]).newInstance(this);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (presenter == null) {
                try {
                    presenter = pClass.newInstance();
                    presenter.attach(this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        return presenter;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (presenter != null) {
            presenter.detach();
        }
    }
}