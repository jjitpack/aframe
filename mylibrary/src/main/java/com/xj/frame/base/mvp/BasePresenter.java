package com.xj.frame.base.mvp;

import androidx.annotation.NonNull;

public abstract class BasePresenter< V extends IView > {
    public V view;

    public void attach(@NonNull V view) {
        this.view = view;
    }

    public void detach() {
        view = null;
    }

}
