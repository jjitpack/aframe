package com.xj.frame.base;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gyf.immersionbar.ImmersionBar;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.impl.LoadingPopupView;
import com.xj.frame.R;

public abstract class BaseActivity extends AppCompatActivity {
    private RelativeLayout rlTitle;
    private ImageView ivTitleLeft;
    private TextView tvTitle;
    private TextView tvTitleRight;
    private FrameLayout flContainer;

    private LoadingPopupView loadingPop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        initContentView();
        initView();
        initData();
    }

    /**
     * 抽象方法
     */
    protected abstract void initView();

    protected abstract void initData();

    /**
     * 共底层调用
     */
    protected View getContentView() {
        return getLayoutInflater().inflate(0, null);
    }

    /**
     * 组合布局
     */
    private void initContentView() {
        rlTitle = findViewById(R.id.rl_title);
        ivTitleLeft = findViewById(R.id.iv_title_left);
        tvTitle = findViewById(R.id.tv_title);
        tvTitleRight = findViewById(R.id.tv_title_right);
        flContainer = findViewById(R.id.fl_container);

        View view = getContentView();
        if (view != null) {
            flContainer.addView(view, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
        }

        ivTitleLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        //如果activity是全屏状态，不显示状态栏，隐藏状态栏view
        if ((getWindow().getAttributes().flags & WindowManager.LayoutParams.FLAG_FULLSCREEN)
                == WindowManager.LayoutParams.FLAG_FULLSCREEN
                || (getWindow().getAttributes().systemUiVisibility & View.SYSTEM_UI_FLAG_FULLSCREEN)
                == View.SYSTEM_UI_FLAG_FULLSCREEN
                || (getWindow().getDecorView().getSystemUiVisibility() & View.SYSTEM_UI_FLAG_FULLSCREEN)
                == View.SYSTEM_UI_FLAG_FULLSCREEN) {
            findViewById(R.id.status_bar_view).setVisibility(View.GONE);//隐藏状态栏view
        }
        //初始化状态栏
        ImmersionBar.with(this)
                .statusBarView(R.id.status_bar_view)
                .navigationBarColor(R.color.white)
                .navigationBarDarkIcon(true)
                .statusBarDarkFont(true)
                .init();
    }

    /**
     * 头标题
     */
    protected void hideDefaultTitle() {
        rlTitle.setVisibility(View.GONE);
    }

    protected ImageView getIvTitleLeft() {
        return ivTitleLeft;
    }

    protected TextView getTvTitle() {
        return tvTitle;
    }

    protected TextView getTvTitleRight() {
        return tvTitleRight;
    }

    /**
     * 加载框
     */
    protected void showLoadingPop() {
        showLoadingPop(null);
    }

    protected void showLoadingPop(String title) {
        if (loadingPop == null) {
            loadingPop = new XPopup.Builder(this).asLoading(title);
        } else {
            loadingPop.setTitle(title);
        }
        if (!loadingPop.isShow()) loadingPop.show();
    }

    protected void dismissLoadingPop() {
        if (loadingPop != null && loadingPop.isShow()) {
            loadingPop.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (loadingPop != null) {
            loadingPop = null;
        }
    }
}