package com.xj.frame.base;

import androidx.viewbinding.ViewBinding;

import android.view.LayoutInflater;
import android.view.View;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public abstract class BaseVBActivity< VB extends ViewBinding > extends BaseActivity {
    public VB viewBinding;

    @Override
    protected View getContentView() {
        if (viewBinding == null) {
            viewBinding = createBinding();
        }
        if (viewBinding != null) {
            return viewBinding.getRoot();
        }
        return super.getContentView();
    }

    protected VB createBinding() {
        //返回当前类的父类Type,即 BaseVBActivity
        Type type = this.getClass().getGenericSuperclass();
        if (type instanceof ParameterizedType) {
            //如果支持泛型
            try {
                //获得泛型中的实际类型，可能会存在多个泛型，[0]也就是获得VB的type
                Class< VB > clazz = (Class< VB >) ((ParameterizedType) type).getActualTypeArguments()[0];
                //反射inflate
                Method method = clazz.getMethod("inflate", LayoutInflater.class);
                //方法调用,获得ViewBinding实例
                viewBinding = (VB) method.invoke(null, getLayoutInflater());
                return viewBinding;
            } catch (Exception e) {
                e.printStackTrace();
            }
            setContentView(viewBinding.getRoot());
        }
        return null;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (null != viewBinding) {
            viewBinding = null;
        }
    }
}